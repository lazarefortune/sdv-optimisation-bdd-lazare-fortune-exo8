## Exercice 8

### Requirements
- Docker
- Docker-compose

### How to run
- Clone the repository
- Run the following command:
```bash
docker-compose up -d
```
- Open your swagger UI at http://localhost:8081/swagger-ui/index.html
- Enjoy!
- To stop the application, run the following command:
```bash
docker-compose down
```
